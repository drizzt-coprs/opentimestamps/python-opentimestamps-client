Name:           opentimestamps-client
Version:        0.7.1
Release:        1%{?dist}
Summary:        OpenTimestamps client

License:        LGPLv3+
URL:            https://github.com/opentimestamps/%{name}
Source0:        https://github.com/opentimestamps/%{name}/archive/%{name}-v%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel python3-setuptools
# This is really CheckRequires ...
BuildRequires:  python3-opentimestamps
BuildRequires:  python3-appdirs
BuildRequires:  python3-GitPython
BuildRequires:  python3-pysocks
# End of CheckRequires

Provides: python3-%{name}
%{?python_provide:%python_provide python3-%{name}}
%{?python_enable_dependency_generator}

%description
Command-line tool to create and validate timestamp proofs with the
OpenTimestamps protocol, using the Bitcoin blockchain as a timestamp notary.
Additionally this package provides timestamping of PGP signed Git commits,
and verification of timestamps for both Git commits as a whole, and individual
files within a Git repository.

%prep
%autosetup -n %{name}-%{name}-v%{version} -p1

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

# Note that there is no %%files section for the unversioned python module
%files
%license LICENSE
%doc README.md
%{python3_sitelib}/otsclient
%{python3_sitelib}/opentimestamps_client-%{version}-py?.*.egg-info
%{_bindir}/ots
%{_bindir}/ots-git-gpg-wrapper

%changelog
* Mon Oct 17 2022 Timothy Redaelli <tredaelli@redhat.com> - 0.7.1-1
- Rebased to 0.7.1

* Fri Oct 19 2018 Timothy Redaelli <tredaelli@redhat.com> - 0.6.0-2
- Renamed to opentimestamps-client

* Wed Oct 10 2018 Timothy Redaelli <tredaelli@redhat.com> - 0.6.0-1
- Initial packaging

